<!DOCTYPE html>
<html>
<head>
   <title>Food Ordering System</title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
    <style type="text/css">
        .container
        {
            width: 500px;
        }
        body
        {
            background: url('index1.jpg');
            background-size:cover;  
            color: gray;
            font-weight: bold;
            font-size: 20px;
        }
        h2
        {
            color:blue;
        }
        h3
        {
            color:yellow;
        }
        samp
        {
            color: red;
        }
        p
        {
            font-size: 15px;
        }
    </style>
</head>
<body>
    <div>
        <h2 align="center" class="justify-content-center align-self-center">Food Ordering System</h2>      
    </div>
    <hr>
    <br>
    <br>
    <br>
    <h3 align="center">Login <samp> & </samp> Enjoy Your Food...</h3>
    <br>
    <form action="login.php" method="post">
    <div class="container form-group">
        <div>
            <label>Email</label>
            <input class="form-control" type="text" name="email" placeholder="Enter your email"  pattern="^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$">
        </div><br>
        <div>
            <label>Password</label>
            <input class="form-control" type="password" name="pass" placeholder="Password">
        </div><br>
        <div align="center">
            <input type="submit" name="submit" value="Login" onclick="login()" class="btn btn-success">
        </div>
    </div>
    </form>
    <p align="center">I am a <a href="register.php">NewUser</a></p>
<?php
    include "./config.php";
    if(count($_POST)>0)
    {
        $result = mysqli_query($con,"SELECT * FROM users WHERE Email='" . $_POST["email"] . "' and Password = '". $_POST["pass"]."'");
        $count = mysqli_num_rows($result);
        if($count==0) {
            echo " <script> alert('Invalid Username or Password!');</script>";
            echo '<script>window.location="login.php"</script>';
        } 
        else {
            echo '<script>window.location="order.html"</script>';
        }
    }
?>
</body>
</html>

