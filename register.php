<?php
	include "./config.php";
?>

<!DOCTYPE html>
<html>
<head>
	<title>Food Ordering System</title>
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
	<style type="text/css">
		.container
		{
			width: 500px;
		}
		body
        {
            background: url('index1.jpg');
            background-size:cover;  
            color: gray;
            font-weight: bold;
            font-size: 15px;
        }
        h2
        {
        	color:blue;
        }
        h3
        {
        	color:yellow;
        }
	</style>
</head>
<body>
	<div>
    	<h2 align="center" class="justify-content-center align-self-center">Food Ordering System</h2>      
  	</div>
  	
  	<hr>

   	<h3 align="center">For New Users Registration....</h3>
   	<br><br>

  	<form action="register.php" method="post">
	<div class="container form-group">
		<div>
			<input class="form-control" type="text" name="name" placeholder="Enter your full name">
		</div>
		<br>
		<div>
			<div class="form-group">
            	<input type="radio" name="gender" value="Male">
            	<label for="male">Male</label>&nbsp&nbsp
            	<input type="radio" name="gender" value="Female">
            	<label for="female">Female</label>&nbsp&nbsp
            	<input type="radio" name="gender" value="Others">
            	<label for="others">Others</label>
        	</div>
		</div>
		<div>
			<input class="form-control" type="number" name="contact" placeholder="Contact Number">
		</div><br>
		<div>
			<input class="form-control" type="text" name="email" placeholder="Enter your email" pattern="^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$">
		</div><br>
		<div>
			<input class="form-control" type="text" name="address" placeholder="Address">
		</div><br>
		<div>
			<input class="form-control" type="text" name="pincode" placeholder="Your Pin Code">
		</div><br>
		<div>
			<input class="form-control" type="password" name="password1" placeholder="Password">
		</div><br>
		<br>
  		<div align="center">
  			<input type="submit" name="submit" value="Register" onclick="register()" class="btn btn-success">
  		</div>
	</div>
	</form>
    <p align="center">Already Registered <a href="login.php">login</a></p>
<script type="text/javascript">
  function register()
  {
	var name=document.getElementsByName('name').value
	var gender=document.getElementsByName('gender').value
	var contact=document.getElementsByName('contact').value
	var email=document.getElementsByName('email').value
	var address=document.getElementsByName('address').value
	var pass1=document.getElementsByName('password1').value
	var mailformat = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/
	if(mailformat.match(email)!=true)
	{
		console.log('email error')
	}
  }
</script>
<?php 
    if(isset($_POST['submit']))
    {
        $name=$_POST["name"];
        $gender=$_POST["gender"];
        $email=$_POST["email"];
        $contact=$_POST["contact"];
        $address=$_POST["address"];
        $pincode=$_POST["pincode"];
        $password=$_POST["password1"];
        $fsql="INSERT INTO `users` (`Name`, `Gender`, `Email`, `Contact`, `Address`, `PinCode`, `Password`) VALUES ('$name', '$gender', '$email', '$contact','$address', '$pincode', '$password');";
        if($con->query($fsql))
        {
            echo "<script> alert('Added Successfully'); </script>";
            echo "<script>window.location='login.php'</script>";
        }
        else
        {
            echo "<script> alert('Failed to add you'); </script>";
        }
    }
?>
</body>
</body>
</html>